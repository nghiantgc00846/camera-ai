

import React, { useEffect, useMemo, useReducer, useRef } from 'react';
import logo from './logo.svg';
import './App.css';
import protobuf from 'protobufjs';
declare global {
  interface Window {
    cv?: any;
  }
}

function App() {
  let imagesRef: any = useRef<string[]>([]);
  const [, forceUpdate] = useReducer(x => x + 1, 0)
  useMemo(async () => {
    const socket = new WebSocket("wss://10.10.60.212:9002");
    // Connection opened
    socket.onopen = () => {
      console.log("Connected to Server");
      socket.send("Hello Server!");
    };

    const root = await protobuf.load('message.proto');

    // Listen for messages
    socket.onmessage = async (res) => {
      console.log("Message from server:", res);
      const uint8Array = new Uint8Array(await res.data.arrayBuffer());

      // Define message types
      const messageTypeMap: any = {
        WrapperMessage: root.lookupType('WrapperMessage'),
        FaceUnknown: root.lookupType('FaceUnknown'),
        FaceDetected: root.lookupType('FaceDetected'),
        RegisterFace: root.lookupType('RegisterFace')
      };

      // Decode the message
      let messageObject;
      let messageType: any;
      for (const entries of Object.entries(messageTypeMap)) {
        let typeName: any = entries[0];
        let MessageType: any = entries[1];
        try {
          messageObject = MessageType.decode(uint8Array);
          messageType = typeName;

          break;
        } catch (error) {
          console.error(`Error decoding ${typeName} Protobuf message:`, error);
        }
      }

      if (messageObject) {
        // Convert message to object
        const object = messageTypeMap[messageType].toObject(messageObject, {
          longs: String,
          enums: String,
          bytes: String,
          // see ConversionOptions
        });
        if (object && object.faceUnknown) {
          imagesRef.current = [...imagesRef.current, object.faceUnknown.image];
          forceUpdate();
        }
        console.log(`Message data (${messageType}):`, object);
      }
    };
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <div style={{ flexDirection: 'row', flexWrap: "wrap" }}>
          {imagesRef?.current?.map((item: any, index: any) =>
            <img src={item} key={index} />
          )}
        </div>
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
